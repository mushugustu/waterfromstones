# Databricks notebook source
# MAGIC %md
# MAGIC <h1>Transform Spark code to R code</h1>

# COMMAND ----------

# MAGIC %py
# MAGIC #Use the User interface to load the file SKU01 into the next location: /FileStore/tables/SKU01.csv
# MAGIC # File location and type
# MAGIC file_location = "/FileStore/tables/SKU01.csv"
# MAGIC file_type = "csv"
# MAGIC 
# MAGIC # CSV options
# MAGIC infer_schema = "false"
# MAGIC first_row_is_header = "true"
# MAGIC delimiter = ","
# MAGIC # The applied options are for CSV files. For other file types, these will be ignored.
# MAGIC df = spark.read.format(file_type) \
# MAGIC   .option("inferSchema", infer_schema) \
# MAGIC   .option("header", first_row_is_header) \
# MAGIC   .option("sep", delimiter) \
# MAGIC   .load(file_location)
# MAGIC #Create a temp table in memory
# MAGIC df.createOrReplaceTempView("sku01raw")

# COMMAND ----------

#Import library to load an R data frame from a spark dataframe
library(SparkR)
sku01rawsparkr <- sql("select * from sku01raw")
#Transform from sparkr to r data.frame
sku01raw <- collect(sku01rawsparkr)
display(sku01raw)

# COMMAND ----------

class(sku01raw)

# COMMAND ----------

# MAGIC %md
# MAGIC <h1>From here we can work with R code<h1>
# MAGIC   <h2> Transform the dataset into a time series of 36 points<h2>

# COMMAND ----------

# Get the dimensions of the dataset
totalCols <- ncol(sku01raw)
totalRows <- nrow(sku01raw)
# Get the first row, transposed
asStringTs <- t(sku01raw[1,][,3:totalCols])
# Iterate over the dataset
# Loop over my_matrix to concatenate the three years
for(i in 2:totalRows) {
  asStringTs <- append(asStringTs, t(sku01raw[i,][,3:totalCols]))
  }
asStringTs
asNumbersTs <- as.numeric(gsub(",","",asStringTs))
asNumbersTs
#Display the data
plot(asNumbersTs, main="SKU01", 
     xlab="Month", ylab="Demand", pch=19, type="b")

# COMMAND ----------

m <- cbind(asNumbersTs[5:10],asNumbersTs[6:11])
m

# COMMAND ----------

lagnum <- 4
total<-length(asNumbersTs)
abt<-asNumbersTs[1:(total-lagnum)]
abt

# COMMAND ----------

# DBTITLE 1,Generate Analytical Base Table
# Autoregresive table
lagnum <- 5
total<-length(asNumbersTs)
endIndex <- (total-lagnum)
startIndex <- 1
abt<-asNumbersTs[startIndex:endIndex]
for(i in 1:(lagnum-1)) {
  abt<- cbind(abt, asNumbersTs[(startIndex+i):(endIndex-i)] )
  }
abt

# COMMAND ----------

#Export ABT to Data Frame in order to be able to download it
df <- as.DataFrame(as.data.frame(abt))
display(df)

# COMMAND ----------

display(df)

# COMMAND ----------

# DBTITLE 1,Run ML model for prediction


# COMMAND ----------



# COMMAND ----------



# COMMAND ----------

round(4/3)

# COMMAND ----------

length(asNumbersTs)

# COMMAND ----------



# COMMAND ----------

asNumbersTs
